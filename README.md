# Sesimic Vision Search

Notebook to create a Seismic Vision search application with Amazon SageMaker.

## Background
Vision Search or Content-based image similarity retrieval is important for upstream oil and gas, as it allows explorers to quickly and automatically find geological analogues in other plays to perform case-based reasoning.In this demo, Amazon SageMaker along with Keras and Annoy will be used to create 2D seismic image retrival program.
Seismic images will be either Faults or Salts from Virtual Seismic Atlas, with the end goal of search returning relevant images in each category.